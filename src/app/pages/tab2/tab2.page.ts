import { Component } from '@angular/core';
import { Register } from 'src/app/models/register.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  constructor(public data: DataService) {}

  sendEmail() {
    console.log('Enviado correo');
  }

  openRegister(register: Register) {
    this.data.openRegister(register);
  }
}
