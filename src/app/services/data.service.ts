import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NavController } from '@ionic/angular';
import { Register } from '../models/register.model';

import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  registers: Register[] = [];

  constructor(
    private storage: Storage,
    private navController: NavController,
    private inAppBrowser: InAppBrowser
  ) {
    this.storage.create();
    this.loadStorage();

  }

  async loadStorage() {
    this.registers = (await this.storage.get('registros')) || [];
  }

  async saveRegister(format: string, text: string) {
    await this.loadStorage();
    const newRegister = new Register(format, text);
    this.registers.unshift(newRegister);

    this.storage.set('registros', this.registers);
  }

  openRegister(register: Register) {
    const http = 'http';
    const geo = 'geo';
    this.navController.navigateForward('tabs/tab2');

    switch (register.type) {
      case http:
        this.inAppBrowser.create(register.text, '_system');
        break;
      case geo:
        this.navController.navigateForward(`/tabs/tab2/maps/${register.text}`);
        break;
    }
  }
}
